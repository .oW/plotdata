﻿using Plot;
using Plus;
using System;
using System.Collections.Generic;

namespace MltiLanguage
{
    class Program
    {
        static void Main(string[] args)
        {
            StringPlus.Instance.SetGetYAndString<Vector2>(Vector2ByString, StringByVector2);

            try
            {
                int i = 0;
                int j = 0;

                string plotPath = args[i++];
                string languagePath = args[i++];

                string idStr = args[i++];

                List<int> values = GetStrValue(idStr);

                LanguageManage.Instance.InitData(plotPath);
                LanguageManage.Instance.ChangeLanguage(languagePath, values[j++], values[j++], values[j++], values[j++], values[j++], values[j++], values[j++], values[j++]);
            }
            catch (Exception e) 
            {
                Console.WriteLine(e);
            }
        }

        public static List<int> GetStrValue(string str)
        {
            List<int> values = new List<int>();


            string[] idStrs = str.Split("+");

            for (int i = 0; i < idStrs.Length; i++)
            {
                string[] ids = idStrs[i].Split("-");
                for (int j = 0; j < ids.Length; j++)
                {
                    string id = ids[j];
                    Console.WriteLine(id);
                    values.Add(int.Parse(ids[j]));
                }
            }

            return values;
        }

        #region Vector2

        public static Vector2 Vector2ByString(string str)
        {
            string[] strs = StringPlus.SplitField(str);
            Vector2 v2 = Vector2.zero;
            if (strs.Length >= 2)
            {
                v2.x = StringPlus.FloatByString(strs[0]);
                v2.y = StringPlus.FloatByString(strs[1]);
            }

            return v2;
        }
        public static string StringByVector2(Vector2 v2)
        {
            string str = "";
            if (v2 != Vector2.zero)
            {
                str = StringPlus.SpliceField(v2.x, v2.y);
            }
            return str;
        }

        #endregion
    }
}
