﻿using Data;
using Plus.Data;
using Plus.GIO;
using Plus.GIO.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MltiLanguage
{
    public class LanguageManage : Plus.Singleton<LanguageManage>
    {

        public const string newFunction = "NewFunction";

        List<string> titles = new List<string>();
        List<string> names = new List<string>();
        List<string> texts = new List<string>();
        List<string> Options = new List<string>();

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="plotPath"></param>
        /// <param name="languagePath"></param>
        public void InitData(string plotPath)
        {

            List<FileData> csvDatas = DirectoryPlus.GetTypes(DirectoryType.All, FileType.csv, plotPath);
            for (int i = csvDatas.Count - 1; i >= 0; i--)
            {
                if (!csvDatas[i].name.StartsWith(newFunction))
                {
                    csvDatas.RemoveAt(i);
                }
            }
            for (int i = 0; i < csvDatas.Count; i++)
            {
                FileData item = csvDatas[i];
                List<NewFunctionCSVData> nfs = DataBasePlus.GetList<NewFunctionCSVData>(plotPath, item.name, DirectoryType.Assets);

                for (int j = 0; j < nfs.Count; j++)
                {
                    NewFunctionCSVData itemN = nfs[j];

                    AddTxt(ref titles, itemN.Title);
                    AddTxt(ref names, itemN.Speaker_name);
                    AddTxt(ref texts, itemN.Conversation_content);
                    AddTxt(ref Options, itemN.Options_11);
                    AddTxt(ref Options, itemN.Options_21);
                    AddTxt(ref Options, itemN.Options_31);
                    AddTxt(ref Options, itemN.Options_41);
                    AddTxt(ref Options, itemN.Options_51);
                    AddTxt(ref Options, itemN.Options_61);
                }
            }
        }

        /// <summary>
        /// 添加文本
        /// </summary>
        /// <param name="texts"></param>
        /// <param name="text"></param>
        public void AddTxt(ref List<string> texts,string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                if (texts.IndexOf(text) == -1)
                {
                    texts.Add(text);
                }
            }
        }

        /// <summary>
        /// 转换
        /// </summary>
        /// <param name="languagePath">路径</param>
        /// <param name="titleIdStart"></param>
        /// <param name="titleIdEnd"></param>
        /// <param name="OptionsStart"></param>
        /// <param name="OptionsEnd"></param>
        /// <param name="nameIdStart"></param>
        /// <param name="nameIdEnd"></param>
        /// <param name="textIdStart"></param>
        /// <param name="textIdEnd"></param>
        public void ChangeLanguage(string languagePath, int titleIdStart, int titleIdEnd,int OptionsStart, int OptionsEnd, int nameIdStart,int nameIdEnd,int textIdStart,int textIdEnd)
        {
            Dictionary<int, languageCNCSVData> langCN = DataBasePlus.GetDicInt<languageCNCSVData>(languagePath);

            List<int> keys = langCN.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                int key = keys[i];
                JudgmentRemove(ref langCN, titleIdStart, titleIdEnd, key);
                JudgmentRemove(ref langCN, OptionsStart, OptionsEnd, key);
                JudgmentRemove(ref langCN, nameIdStart, nameIdEnd, key);
                JudgmentRemove(ref langCN, textIdStart, textIdEnd, key);
            }
            AddlanguageData(ref langCN, titles, titleIdStart);
            AddlanguageData(ref langCN, Options, OptionsStart);
            AddlanguageData(ref langCN, names, nameIdStart);
            AddlanguageData(ref langCN, texts, textIdStart);
            Dictionary<int, languageCNCSVData> newCn =DataBasePlus.SortDictionaryT(langCN);
            DataBasePlus.SaveData(newCn, languagePath,DirectoryType.Assets);
            Console.WriteLine("多语言保存成功");
        }

        public void JudgmentRemove(ref Dictionary<int, languageCNCSVData> langCN, int start,int end,int key)
        {
            if (end >= key && key >= start)
            {
                langCN.Remove(key);
            }
        }

        public void AddlanguageData(ref Dictionary<int, languageCNCSVData> langCN, List<string> texts, int start)
        {
            for (int i = 0; i < texts.Count; i++)
            {
                languageCNCSVData data = new languageCNCSVData();
                data.Id = start + i;
                data.Uid = data.Id;
                data.Text = texts[i].Replace("\"","\\\"");
                langCN.Add(data.Id, data);
            }
        }
    }
}
