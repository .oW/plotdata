using Plot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Data
{
	public class func_roleCSVData : CSVDataBase
	{
		//小节id
		private int section_id;
		//场次
		private int session_id;
		//图片id
		private int photo_id;
		//起始位置
		private int starting_point;
		//立绘的缩放
		private Vector2 photo_size;
		//淡入淡出特效
		private int inout_Effect;
		//特效
		private List<int> effect;
		//下一个锚点位置
		private int next_anchor_position;
		//下一个锚点的对应位置
		private Vector2 next_anchor_private_position;
		//移动的时间
		private float moving_time;
		//淡入淡出的值
		private float alpha_value;
		//淡入淡出的时间
		private float alpha_changer_time;
		//放大的倍数
		private float magnification;
		//放大的时间
		private float magnification_time;
		//左差值
		private float lift_value;
		//右差值
		private float right_value;
		//左右跳动的时间
		private float lift_right_time;
		//上差值
		private float up_value;
		//下差值
		private float down_value;
		//上下跳动的时间
		private float up_down_time;
		//小表情id
		private int emoticon_id;
		//小表情对应位置
		private Vector2 emoticon_position;
		//小表情对应的缩放
		private Vector2 emoticon_scale;
		//面部表情id
		private int facial_expression;
		//面部表情对应位置
		private Vector2 facial_emoticon_position;
		/// <summary>
		/// 小节id
		/// </summary>
		public int Section_id { get => section_id; set => section_id = value; }
		/// <summary>
		/// 场次
		/// </summary>
		public int Session_id { get => session_id; set => session_id = value; }
		/// <summary>
		/// 图片id
		/// </summary>
		public int Photo_id { get => photo_id; set => photo_id = value; }
		/// <summary>
		/// 起始位置
		/// </summary>
		public int Starting_point { get => starting_point; set => starting_point = value; }
		/// <summary>
		/// 立绘的缩放
		/// </summary>
		public Vector2 Photo_size { get => photo_size; set => photo_size = value; }
		/// <summary>
		/// 淡入淡出特效
		/// </summary>
		public int Inout_Effect { get => inout_Effect; set => inout_Effect = value; }
		/// <summary>
		/// 特效
		/// </summary>
		public List<int> Effect { get => effect; set => effect = value; }
		/// <summary>
		/// 下一个锚点位置
		/// </summary>
		public int Next_anchor_position { get => next_anchor_position; set => next_anchor_position = value; }
		/// <summary>
		/// 下一个锚点的对应位置
		/// </summary>
		public Vector2 Next_anchor_private_position { get => next_anchor_private_position; set => next_anchor_private_position = value; }
		/// <summary>
		/// 移动的时间
		/// </summary>
		public float Moving_time { get => moving_time; set => moving_time = value; }
		/// <summary>
		/// 淡入淡出的值
		/// </summary>
		public float Alpha_value { get => alpha_value; set => alpha_value = value; }
		/// <summary>
		/// 淡入淡出的时间
		/// </summary>
		public float Alpha_changer_time { get => alpha_changer_time; set => alpha_changer_time = value; }
		/// <summary>
		/// 放大的倍数
		/// </summary>
		public float Magnification { get => magnification; set => magnification = value; }
		/// <summary>
		/// 放大的时间
		/// </summary>
		public float Magnification_time { get => magnification_time; set => magnification_time = value; }
		/// <summary>
		/// 左差值
		/// </summary>
		public float Lift_value { get => lift_value; set => lift_value = value; }
		/// <summary>
		/// 右差值
		/// </summary>
		public float Right_value { get => right_value; set => right_value = value; }
		/// <summary>
		/// 左右跳动的时间
		/// </summary>
		public float Lift_right_time { get => lift_right_time; set => lift_right_time = value; }
		/// <summary>
		/// 上差值
		/// </summary>
		public float Up_value { get => up_value; set => up_value = value; }
		/// <summary>
		/// 下差值
		/// </summary>
		public float Down_value { get => down_value; set => down_value = value; }
		/// <summary>
		/// 上下跳动的时间
		/// </summary>
		public float Up_down_time { get => up_down_time; set => up_down_time = value; }
		/// <summary>
		/// 小表情id
		/// </summary>
		public int Emoticon_id { get => emoticon_id; set => emoticon_id = value; }
		/// <summary>
		/// 小表情对应位置
		/// </summary>
		public Vector2 Emoticon_position { get => emoticon_position; set => emoticon_position = value; }
		/// <summary>
		/// 小表情对应的缩放
		/// </summary>
		public Vector2 Emoticon_scale { get => emoticon_scale; set => emoticon_scale = value; }
		/// <summary>
		/// 面部表情id
		/// </summary>
		public int Facial_expression { get => facial_expression; set => facial_expression = value; }
		/// <summary>
		/// 面部表情对应位置
		/// </summary>
		public Vector2 Facial_emoticon_position { get => facial_emoticon_position; set => facial_emoticon_position = value; }
		public override int GetImportantId => Section_id;
		public func_roleCSVData()
		{
		}
		protected override void OnSet(List<string> list)
		{
			int i = 4;
			section_id = sP.TByString<int>(list[++i]);
			session_id = sP.TByString<int>(list[++i]);
			photo_id = sP.TByString<int>(list[++i]);
			starting_point = sP.TByString<int>(list[++i]);
			photo_size = sP.TByString<Vector2>(list[++i]);
			inout_Effect = sP.TByString<int>(list[++i]);
			effect = sP.ListByString<int>(list[++i]);
			next_anchor_position = sP.TByString<int>(list[++i]);
			next_anchor_private_position = sP.TByString<Vector2>(list[++i]);
			moving_time = sP.TByString<float>(list[++i]);
			alpha_value = sP.TByString<float>(list[++i]);
			alpha_changer_time = sP.TByString<float>(list[++i]);
			magnification = sP.TByString<float>(list[++i]);
			magnification_time = sP.TByString<float>(list[++i]);
			lift_value = sP.TByString<float>(list[++i]);
			right_value = sP.TByString<float>(list[++i]);
			lift_right_time = sP.TByString<float>(list[++i]);
			up_value = sP.TByString<float>(list[++i]);
			down_value = sP.TByString<float>(list[++i]);
			up_down_time = sP.TByString<float>(list[++i]);
			emoticon_id = sP.TByString<int>(list[++i]);
			emoticon_position = sP.TByString<Vector2>(list[++i]);
			emoticon_scale = sP.TByString<Vector2>(list[++i]);
			facial_expression = sP.TByString<int>(list[++i]);
			facial_emoticon_position = sP.TByString<Vector2>(list[++i]);
		}
		protected override List<string> OnGet(List<string> list)
		{
			list.Add(sP.StringByT(section_id));
			list.Add(sP.StringByT(session_id));
			list.Add(sP.StringByT(photo_id));
			list.Add(sP.StringByT(starting_point));
			list.Add(sP.StringByT(photo_size));
			list.Add(sP.StringByT(inout_Effect));
			list.Add(sP.StringByList(effect));
			list.Add(sP.StringByT(next_anchor_position));
			list.Add(sP.StringByT(next_anchor_private_position));
			list.Add(sP.StringByT(moving_time));
			list.Add(sP.StringByT(alpha_value));
			list.Add(sP.StringByT(alpha_changer_time));
			list.Add(sP.StringByT(magnification));
			list.Add(sP.StringByT(magnification_time));
			list.Add(sP.StringByT(lift_value));
			list.Add(sP.StringByT(right_value));
			list.Add(sP.StringByT(lift_right_time));
			list.Add(sP.StringByT(up_value));
			list.Add(sP.StringByT(down_value));
			list.Add(sP.StringByT(up_down_time));
			list.Add(sP.StringByT(emoticon_id));
			list.Add(sP.StringByT(emoticon_position));
			list.Add(sP.StringByT(emoticon_scale));
			list.Add(sP.StringByT(facial_expression));
			list.Add(sP.StringByT(facial_emoticon_position));
			return list;
		}
	}
}
