using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Data
{
	public class languageCNCSVData : CSVDataBase
	{
		//简体
		private string text;
		/// <summary>
		/// 简体
		/// </summary>
		public string Text { get => text; set => text = value; }
		public override string GetImportantName => Text;
		public languageCNCSVData()
		{
		}
		protected override void OnSet(List<string> list)
		{
			int i = 4;
			text = sP.TByString<string>(list[++i]);
		}
		protected override List<string> OnGet(List<string> list)
		{
			list.Add(sP.StringByT(text));
			return list;
		}
	}
}
