using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Data
{
	public class languageTWCSVData : CSVDataBase
	{
		//繁體
		private string text;
		/// <summary>
		/// 繁體
		/// </summary>
		public string Text { get => text; set => text = value; }
		public languageTWCSVData()
		{
		}
		protected override void OnSet(List<string> list)
		{
			int i = 4;
			text = sP.TByString<string>(list[++i]);
		}
		protected override List<string> OnGet(List<string> list)
		{
			list.Add(sP.StringByT(text));
			return list;
		}
	}
}
