using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Data
{
	public class func_cgCSVData : CSVDataBase
	{
		//小节id
		private int section_id;
		//场次
		private int session_id;
		//特效类型
		private int effect_type;
		//spine动画id
		private int type_spine;
		//动画id
		private int type_anim;
		//特效
		private int effect;
		//cg图片id
		private int cg_image_id;
		/// <summary>
		/// 小节id
		/// </summary>
		public int Section_id { get => section_id; set => section_id = value; }
		/// <summary>
		/// 场次
		/// </summary>
		public int Session_id { get => session_id; set => session_id = value; }
		/// <summary>
		/// 特效类型
		/// </summary>
		public int Effect_type { get => effect_type; set => effect_type = value; }
		/// <summary>
		/// spine动画id
		/// </summary>
		public int Type_spine { get => type_spine; set => type_spine = value; }
		/// <summary>
		/// 动画id
		/// </summary>
		public int Type_anim { get => type_anim; set => type_anim = value; }
		/// <summary>
		/// 特效
		/// </summary>
		public int Effect { get => effect; set => effect = value; }
		/// <summary>
		/// cg图片id
		/// </summary>
		public int Cg_image_id { get => cg_image_id; set => cg_image_id = value; }
		public override int GetImportantId => Section_id;
		public func_cgCSVData()
		{
		}
		protected override void OnSet(List<string> list)
		{
			int i = 4;
			section_id = sP.TByString<int>(list[++i]);
			session_id = sP.TByString<int>(list[++i]);
			effect_type = sP.TByString<int>(list[++i]);
			type_spine = sP.TByString<int>(list[++i]);
			type_anim = sP.TByString<int>(list[++i]);
			effect = sP.TByString<int>(list[++i]);
			cg_image_id = sP.TByString<int>(list[++i]);
		}
		protected override List<string> OnGet(List<string> list)
		{
			list.Add(sP.StringByT(section_id));
			list.Add(sP.StringByT(session_id));
			list.Add(sP.StringByT(effect_type));
			list.Add(sP.StringByT(type_spine));
			list.Add(sP.StringByT(type_anim));
			list.Add(sP.StringByT(effect));
			list.Add(sP.StringByT(cg_image_id));
			return list;
		}
	}
}
