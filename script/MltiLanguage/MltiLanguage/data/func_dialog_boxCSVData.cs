using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Data
{
	public class func_dialog_boxCSVData : CSVDataBase
	{
		//小节id
		private int section_id;
		//场次id
		private int session_id;
		//对话框图片id
		private int box_img_id;
		//人名框的图片
		private int name_img_id;
		//人名框的位置
		private int name_box_po;
		//说话人的id
		private int speaker_id;
		//当前说话人的名称
		private string speaker_name;
		//头像id
		private int avatar_id;
		//对话内容
		private string conversation_content;
		//对话内容字号
		private int content_size;
		//特效
		private int effect;
		//幅度
		private float amplitude;
		//时间
		private float amplitude_time;
		//配音
		private int dubbing;
		/// <summary>
		/// 小节id
		/// </summary>
		public int Section_id { get => section_id; set => section_id = value; }
		/// <summary>
		/// 场次id
		/// </summary>
		public int Session_id { get => session_id; set => session_id = value; }
		/// <summary>
		/// 对话框图片id
		/// </summary>
		public int Box_img_id { get => box_img_id; set => box_img_id = value; }
		/// <summary>
		/// 人名框的图片
		/// </summary>
		public int Name_img_id { get => name_img_id; set => name_img_id = value; }
		/// <summary>
		/// 人名框的位置
		/// </summary>
		public int Name_box_po { get => name_box_po; set => name_box_po = value; }
		/// <summary>
		/// 说话人的id
		/// </summary>
		public int Speaker_id { get => speaker_id; set => speaker_id = value; }
		/// <summary>
		/// 当前说话人的名称
		/// </summary>
		public string Speaker_name { get => speaker_name; set => speaker_name = value; }
		/// <summary>
		/// 头像id
		/// </summary>
		public int Avatar_id { get => avatar_id; set => avatar_id = value; }
		/// <summary>
		/// 对话内容
		/// </summary>
		public string Conversation_content { get => conversation_content; set => conversation_content = value; }
		/// <summary>
		/// 对话内容字号
		/// </summary>
		public int Content_size { get => content_size; set => content_size = value; }
		/// <summary>
		/// 特效
		/// </summary>
		public int Effect { get => effect; set => effect = value; }
		/// <summary>
		/// 幅度
		/// </summary>
		public float Amplitude { get => amplitude; set => amplitude = value; }
		/// <summary>
		/// 时间
		/// </summary>
		public float Amplitude_time { get => amplitude_time; set => amplitude_time = value; }
		/// <summary>
		/// 配音
		/// </summary>
		public int Dubbing { get => dubbing; set => dubbing = value; }
		public override int GetImportantId => Section_id;
		public func_dialog_boxCSVData()
		{
		}
		protected override void OnSet(List<string> list)
		{
			int i = 4;
			section_id = sP.TByString<int>(list[++i]);
			session_id = sP.TByString<int>(list[++i]);
			box_img_id = sP.TByString<int>(list[++i]);
			name_img_id = sP.TByString<int>(list[++i]);
			name_box_po = sP.TByString<int>(list[++i]);
			speaker_id = sP.TByString<int>(list[++i]);
			speaker_name = sP.TByString<string>(list[++i]);
			avatar_id = sP.TByString<int>(list[++i]);
			conversation_content = sP.TByString<string>(list[++i]);
			content_size = sP.TByString<int>(list[++i]);
			effect = sP.TByString<int>(list[++i]);
			amplitude = sP.TByString<float>(list[++i]);
			amplitude_time = sP.TByString<float>(list[++i]);
			dubbing = sP.TByString<int>(list[++i]);
		}
		protected override List<string> OnGet(List<string> list)
		{
			list.Add(sP.StringByT(section_id));
			list.Add(sP.StringByT(session_id));
			list.Add(sP.StringByT(box_img_id));
			list.Add(sP.StringByT(name_img_id));
			list.Add(sP.StringByT(name_box_po));
			list.Add(sP.StringByT(speaker_id));
			list.Add(sP.StringByT(speaker_name));
			list.Add(sP.StringByT(avatar_id));
			list.Add(sP.StringByT(conversation_content));
			list.Add(sP.StringByT(content_size));
			list.Add(sP.StringByT(effect));
			list.Add(sP.StringByT(amplitude));
			list.Add(sP.StringByT(amplitude_time));
			list.Add(sP.StringByT(dubbing));
			return list;
		}
	}
}
