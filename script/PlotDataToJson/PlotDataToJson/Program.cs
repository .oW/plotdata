﻿using Plot;
using Plus;
using System;

namespace PlotDataToJson
{
    class Program
    {
        static void Main(string[] args)
        {
            StringPlus.Instance.SetGetYAndString<Vector2>(Vector2ByString, StringByVector2);

            try
            {
                Console.WriteLine(args.Length);
                string rPath = args[0];
                string wPath = args[1];
                string flag = args[2];
                LanguageManage.Instance.Init(rPath);
                ConvertDataEditorClass.DataConversion(rPath, wPath, flag, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.WriteLine("生成正确");
        }
        #region Vector2

        public static Vector2 Vector2ByString(string str)
        {
            string[] strs = StringPlus.SplitField(str);
            Vector2 v2 = Vector2.zero;
            if (strs.Length >= 2)
            {
                v2.x = StringPlus.FloatByString(strs[0]);
                v2.y = StringPlus.FloatByString(strs[1]);
            }

            return v2;
        }
        public static string StringByVector2(Vector2 v2)
        {
            string str = "";
            if (v2 != Vector2.zero)
            {
                str = StringPlus.SpliceField(v2.x, v2.y);
            }
            return str;
        }

        #endregion
    }
}
