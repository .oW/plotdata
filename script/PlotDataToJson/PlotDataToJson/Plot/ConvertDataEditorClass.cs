﻿using Data;
using Plot;
using Plus;
using Plus.Data;
using Plus.GIO;
using Plus.GIO.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ConvertDataEditorClass
{
    public const string Save = "[{0}]保存成功";

    #region 常量

    #region 角色常量

    /// <summary>
    /// 角色淡入淡出的时间
    /// </summary>
    private const float Role1_Alpha_changer_time = 1 - 0.1f;
    /// <summary>
    /// 角色移动的时间
    /// </summary>
    private const float Role1_Moving_time = 1 - 0.1f;

    #endregion

    public const string newFunction = "NewFunction";
    #endregion

    /// <summary>
    /// 将现有的
    /// csv
    /// 转换为
    /// json
    /// </summary>
    /// <param name="readPath">读取路径</param>
    /// <param name="writePath">写入的路径</param>
    /// <param name="isLanguage">是否使用多语言</param>
    public static void DataConversion(string readPath, string writePath, string flag, bool isLanguage = false)
    {

        List<FileData> csvDatas = DirectoryPlus.GetTypes(DirectoryType.All, FileType.csv, readPath);
        for (int i = csvDatas.Count - 1; i >= 0; i--)
        {
            if (!csvDatas[i].name.StartsWith(newFunction))
            {
                csvDatas.RemoveAt(i);
            }
        }
        foreach (var item in csvDatas)
        {
            List<NewFunctionCSVData> nfs = DataBasePlus.GetList<NewFunctionCSVData>(readPath, item.name, DirectoryType.Assets);
            PlotDataByGameDesigner(nfs, readPath, writePath, flag,isLanguage);
        }
    }

    #region 剧情

    #region 策划 转 程序


    /// <summary>
    /// 剧情数据从策划编写转为程序所需数据
    /// </summary>
    /// <param name="readPath">读取路径</param>
    /// <param name="writePath">写入的路径</param>
    /// <param name="isLanguage">是否使用多语言</param>
    public static void PlotDataByGameDesigner(List<NewFunctionCSVData> nfs ,string readPath,string writePath, string flag,bool isLanguage = false)
    {
        PlotData data = PlotData.Instance;

        //List<NewFunctionCSVData> nfs = DataBasePlus.GetListByCSV<NewFunctionCSVData>(readPath);

        Dictionary<int, List<dia_sessionCSVData>> dSes = new Dictionary<int, List<dia_sessionCSVData>>();

        Dictionary<int, List<func_bgCSVData>> fBg = new Dictionary<int, List<func_bgCSVData>>();
        Dictionary<int, List<func_cgCSVData>> fCg = new Dictionary<int, List<func_cgCSVData>>();
        Dictionary<int, List<func_dialog_boxCSVData>> fDBox = new Dictionary<int, List<func_dialog_boxCSVData>>();
        Dictionary<int, List<func_roleCSVData>> fRole = new Dictionary<int, List<func_roleCSVData>>();

        //剧情备份
        Dictionary<int, List<NewFunctionCSVData>> nfss = new Dictionary<int, List<NewFunctionCSVData>>();

        data.session.Clear();
        data.funcBg.Clear();
        data.funcCg.Clear();
        data.funcDialogBox.Clear();
        data.funcRole.Clear();

        int count = nfs.Count;
        //角色的索引
        int roleIndex = 1;
        //角色的id
        int roleId = 4;
        //cg的索引
        int cgIndex = 1;
        //cg的id
        int cgId = 4;
        int OldSection_id = -1;
        int sectionId = 4;

        for (int i = 0; i < count; i++)
        {
            NewFunctionCSVData nf = nfs[i];

            if (nf != null)
            {
                DicAdd(ref nfss, nf.Section_id, nf);

                int sec = nf.Section_id;

                dia_sessionCSVData dses = GetDiaSessionData(nf);

                func_bgCSVData fbg = GetFuncBg(nf, isLanguage);
                func_cgCSVData fcg = GetFuncCg(nf, ref cgId, ref cgIndex);
                func_dialog_boxCSVData box = GetFuncDialogBox(nf, isLanguage);
                func_roleCSVData role1 = GetRole1(nf, ref roleId, ref roleIndex);
                func_roleCSVData role2 = GetRole2(nf, ref roleId, ref roleIndex);
                func_roleCSVData role3 = GetRole3(nf, ref roleId, ref roleIndex);

                DicAdd(ref dSes, dses.GetImportantId, dses);

                DicAdd(ref fBg, sec, fbg);
                DicAdd(ref fCg, sec, fcg);
                DicAdd(ref fDBox, sec, box);
                if (role1 != null)
                {
                    DicAdd(ref fRole, sec, role1);
                }
                if (role2 != null)
                {
                    DicAdd(ref fRole, sec, role2);
                }
                if (role3 != null)
                {
                    DicAdd(ref fRole, sec, role3);
                }
            }
        }

        data.session = dSes;

        data.funcBg = fBg;
        data.funcCg = fCg;
        data.funcDialogBox = fDBox;
        data.funcRole = fRole;

        //data.SavePlotChapterData(nfss);
        data.SaveData(writePath, flag);

    }

    #region 对话目录

    /// <summary>
    /// 整理
    /// 场次
    /// </summary>
    /// <param name="data"></param>
    /// <param name="index"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public static dia_sessionCSVData GetDiaSessionData(NewFunctionCSVData data)
    {
        dia_sessionCSVData dses = new dia_sessionCSVData();

        dses.Id = data.Id;
        dses.Uid = data.Uid;
        dses.Section_id = data.Section_id;
        dses.Index = data.Session_id;

        //立绘或CG
        //dses.Role_or_cg = data.Cg_image_id;
        if (data.Effect_type == 9 && (data.Photo_id1 != 0 || data.Photo_id2 != 0 || data.Photo_id3 != 0))
        {
            dses.Role_or_cg = 3;
        }
        else if (data.Effect_type != 0)
        {
            dses.Role_or_cg = 2;
        }
        else if (data.Photo_id1 != 0 || data.Photo_id2 != 0 || data.Photo_id3 != 0)
        {
            dses.Role_or_cg = 1;
        }
        else
        {
            dses.Role_or_cg = 0;
        }
        //Debug.LogError(string.Format("{0}_____{1}_____{2}_____{3}", data.Effect_type, data.Photo_id1, data.Photo_id2, data.Photo_id3));

        dses.Next_index = data.Next_id;

        if (dses.Nexts == null)
        {
            dses.Nexts = new List<int>();
        }
        if (dses.Options1 == null)
        {
            dses.Options1 = new List<string>();
        }

        AddOptions(dses.Nexts, dses.Options1, data.Next_1, data.Options_11);
        AddOptions(dses.Nexts, dses.Options1, data.Next_2, data.Options_21);
        AddOptions(dses.Nexts, dses.Options1, data.Next_3, data.Options_31);
        AddOptions(dses.Nexts, dses.Options1, data.Next_4, data.Options_41);
        AddOptions(dses.Nexts, dses.Options1, data.Next_5, data.Options_51);
        AddOptions(dses.Nexts, dses.Options1, data.Next_6, data.Options_61);

        return dses;
    }

    /// <summary>
    /// 整理
    /// 小节
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    //private static dia_sectionCSVData GetDiaSectionData(NewFunctionCSVData data, ref int id)
    //{
    //    dia_sectionCSVData dData = new dia_sectionCSVData();

    //    dData.Id = data.Section_id;
    //    dData.Uid = id;
    //    dData.Session_index = data.Session_id;
    //    ++id;

    //    return dData;
    //}

    /// <summary>
    /// 辅助添加选项
    /// </summary>
    /// <param name="nexts"></param>
    /// <param name="options"></param>
    /// <param name="next"></param>
    /// <param name="option"></param>
    public static void AddOptions(List<int> nexts, List<string> options, int next, string option)
    {
        if (next != 0)
        {
            nexts.Add(next);
            
            options.Add(LanguageManage.Instance.GetLanguageStrIdEditor(option));
        }
    }

    #endregion

    #region 逻辑

    /// <summary>
    /// 背景
    /// 逻辑
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static func_bgCSVData GetFuncBg(NewFunctionCSVData data, bool isLanguage = false)
    {
        func_bgCSVData fData = null;

        if ((data.Bg_img_id != 0) || (data.Music_id != 0) || (data.Sound_id != 0) || (data.Full_screen_effects != 0) || (data.Title != "" && data.Title != null) || (data.Effect != 0))
        {
            fData = new func_bgCSVData();
            fData.Id = data.Id;
            fData.Uid = data.Uid;
            fData.Section_id = data.Section_id;
            fData.Session_id = data.Session_id;

            fData.Bg_img_id = data.Bg_img_id;
            fData.Music_id = data.Music_id;
            fData.Sound_id = data.Sound_id;
            fData.Full_screen_effects = data.Full_screen_effects;
            fData.Effect = data.Effect;
            fData.Title = isLanguage ? LanguageManage.Instance.GetLanguageStrIdEditor(data.Title) : data.Title;
        }

        return fData;
    }

    /// <summary>
    /// CG
    /// 逻辑
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static func_cgCSVData GetFuncCg(NewFunctionCSVData data, ref int cgId, ref int cgIndex)
    {

        func_cgCSVData fData = null;
        if (data.Effect_type != 0)
        {
            fData = new func_cgCSVData();
            fData.Id = cgIndex;
            fData.Uid = cgId;
            fData.Section_id = data.Section_id;
            fData.Session_id = data.Session_id;

            fData.Effect_type = data.Effect_type;
            fData.Type_spine = data.Type_spine;
            fData.Type_anim = data.Type_anim;
            fData.Cg_image_id = data.Cg_image_id;
            ++cgId;
            ++cgIndex;
        }

        return fData;
    }

    /// <summary>
    /// 对话框
    /// 逻辑
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static func_dialog_boxCSVData GetFuncDialogBox(NewFunctionCSVData data, bool isLanguage = false)
    {
        func_dialog_boxCSVData fData = null;

        if (data.Box_img_id != 0 || (data.Conversation_content != null && data.Conversation_content != ""))
        {
            fData = new func_dialog_boxCSVData();
            fData.Id = data.Id;
            fData.Uid = data.Uid;
            fData.Section_id = data.Section_id;
            fData.Session_id = data.Session_id;

            fData.Box_img_id = data.Box_img_id;
            fData.Effect = data.Box_effect;
            //特效时间
            fData.Amplitude_time = 0.5f;
            fData.Name_box_po = data.Name_box_po;

            fData.Avatar_id = data.Avatar_id;

            fData.Speaker_name = isLanguage ? LanguageManage.Instance.GetLanguageStrIdEditor(data.Speaker_name) : data.Speaker_name;
            fData.Speaker_id = data.Speaker_id;
            fData.Conversation_content = isLanguage ? LanguageManage.Instance.GetLanguageStrIdEditor(data.Conversation_content) : data.Conversation_content;
            fData.Content_size = data.Content_size <= 1 ? 1 : data.Content_size;

            fData.Dubbing = data.Dubbing1;
        }

        return fData;
    }

    /// <summary>
    /// 获取角色
    /// 一
    /// 逻辑
    /// </summary>
    /// <param name="data"></param>
    /// <param name="roleId"></param>
    /// <param name="roleIndex"></param>
    public static func_roleCSVData GetRole1(NewFunctionCSVData data, ref int roleId, ref int roleIndex)
    {

        func_roleCSVData role = null;
        if (data.Photo_id1 != 0)
        {
            role = GetRole(data, ref roleId, ref roleIndex);

            //string
            //role.Photo_id = GetResID(data.imgRole, data.Photo_id1);
            //int
            role.Photo_id = data.Photo_id1;
            role.Starting_point = data.Initial_point1;
            //role.Next_anchor_position = data.Starting_point1;
            //role.Inout_Effect = data.Inout_Effect1;
            //string
            //role.Facial_expression = GetResID(data.imgRoleExpFac, data.Facial_expression1);
            //int
            role.Facial_expression = data.Facial_expression1;

            //string
            //role.Emoticon_id = GetResID(data.imgRoleExpSpc, data.Emoticon_id1);
            //int
            if (data.Emoticon_id1 != 0)
            {
                role.Effect.Add(5);
                role.Emoticon_id = data.Emoticon_id1;
                role.Emoticon_position = data.Emoticon_position1;
            }
            if (data.Photo_effect1 != 0)
            {
                role.Effect.Add(GetRoleEffect(ref role, data.Photo_effect1));
            }
            if (data.Inout_Effect1 != 0)
            {
                RoleAlpha(ref role, data.Inout_Effect1);
                //role.Effect.Add(1);
                //role.Alpha_value = data.Inout_Effect1 % 2;
                //role.Alpha_changer_time = Role1_Alpha_changer_time;
            }
            if (data.Starting_point1 != 0 && data.Initial_point1 != 0)
            {
                RoleNextAnchorPosition(ref role, data.Starting_point1);
                //role.Effect.Add(6);
                //role.Moving_time = Role1_Moving_time;
            }
        }
        return role;
    }

    /// <summary>
    /// 获取角色
    /// 二
    /// 逻辑
    /// </summary>
    /// <param name="data"></param>
    /// <param name="roleId"></param>
    /// <param name="roleIndex"></param>
    /// <returns></returns>
    public static func_roleCSVData GetRole2(NewFunctionCSVData data, ref int roleId, ref int roleIndex)
    {
        func_roleCSVData role = null;

        if (data.Photo_id2 != 0)
        {
            role = GetRole(data, ref roleId, ref roleIndex);

            role.Photo_id = data.Photo_id2;
            role.Starting_point = data.Initial_point2;
            role.Facial_expression = data.Facial_expression2;

            if (data.Emoticon_id2 != 0)
            {
                role.Effect.Add(5);
                role.Emoticon_id = data.Emoticon_id2;
                role.Emoticon_position = data.Emoticon_position2;
            }
            if (data.Photo_effect2 != 0)
            {
                role.Effect.Add(GetRoleEffect(ref role, data.Photo_effect2));
            }
            if (data.Inout_Effect2 != 0)
            {
                RoleAlpha(ref role, data.Inout_Effect2);
            }
            if (data.Starting_point2 != 0 && data.Initial_point2 != 0)
            {
                RoleNextAnchorPosition(ref role, data.Starting_point2);
            }
        }

        return role;
    }

    /// <summary>
    /// 获取角色
    /// 三
    /// 逻辑
    /// </summary>
    /// <param name="data"></param>
    /// <param name="roleId"></param>
    /// <param name="roleIndex"></param>
    /// <returns></returns>
    private static func_roleCSVData GetRole3(NewFunctionCSVData data, ref int roleId, ref int roleIndex)
    {
        func_roleCSVData role = null;

        if (data.Photo_id3 != 0)
        {
            role = GetRole(data, ref roleId, ref roleIndex);

            role.Photo_id = data.Photo_id3;
            role.Starting_point = data.Initial_point3;
            role.Facial_expression = data.Facial_expression3;

            if (data.Emoticon_id3 != 0)
            {
                role.Effect.Add(5);
                role.Emoticon_id = data.Emoticon_id3;
                role.Emoticon_position = data.Emoticon_position3;
            }
            if (data.Photo_effect3 != 0)
            {
                role.Effect.Add(GetRoleEffect(ref role, data.Photo_effect3));
            }
            if (data.Inout_Effect3 != 0)
            {
                RoleAlpha(ref role, data.Inout_Effect3);
            }
            if (data.Starting_point3 != 0 && data.Initial_point3 != 0)
            {
                RoleNextAnchorPosition(ref role, data.Starting_point3);
            }
        }

        return role;
    }

    /// <summary>
    /// 获得角色数据
    /// </summary>
    /// <param name="data"></param>
    /// <param name="roleId"></param>
    /// <param name="roleIndex"></param>
    /// <returns></returns>
    public static func_roleCSVData GetRole(NewFunctionCSVData data, ref int roleId, ref int roleIndex)
    {
        func_roleCSVData role = new func_roleCSVData();

        role.Uid = roleId;
        role.Id = roleIndex;
        role.Section_id = data.Section_id;
        role.Session_id = data.Session_id;
        ++roleId;
        ++roleIndex;
        role.Effect = new List<int>();
        role.Photo_size = new Vector2(1.15f,1.15f);

        return role;
    }

    #endregion

    #endregion

    #region 辅助

    /// <summary>
    /// 策划配表角色特效转程序所需的特效
    /// </summary>
    /// <param name="num"></param>
    public static int GetRoleEffect(ref func_roleCSVData role, int num)
    {
        int nowE = num;
        switch (num)
        {
            case 1://左右震动
                nowE = 2;
                //左幅度
                role.Lift_value = 50f;
                //右幅度
                role.Right_value = 50f;
                //震动时间
                role.Lift_right_time = 0.5f;
                break;
            case 2://上下跳
                nowE = 4;
                //上幅度
                role.Up_value = 50f;
                //下幅度
                role.Down_value = 50f;
                //震动时间
                role.Up_down_time = 0.5f;
                break;
            case 3://放大
                nowE = 3;
                //放大的倍数
                role.Magnification = 1.5f;
                //放大的时间
                role.Magnification_time = 0;
                break;
            case 4://旋转
                nowE = 8;
                break;
        }
        return nowE;
    }

    /// <summary>
    /// 角色透明度特效整理
    /// </summary>
    public static void RoleAlpha(ref func_roleCSVData role, int alpha)
    {
        if (alpha == 1)
        {
            role.Effect.Add(10);
            alpha = 0;
        }
        else if (alpha == 2)
        {
            role.Effect.Add(11);
            alpha = 0;
        }
        else
        {
            //role.Effect.Add(1);
        }
        role.Alpha_value = alpha;
        role.Alpha_changer_time = Role1_Alpha_changer_time;
    }

    /// <summary>
    /// 移动
    /// </summary>
    /// <param name="role"></param>
    /// <param name="next"></param>
    public static void RoleNextAnchorPosition(ref func_roleCSVData role, int next)
    {
        /**
         * 
         * 角色的位置
         * 
         *	    |---------------------------------------|
         *	    |										|
         *	    |										|
         *	1	|	2		3		4		5		6	|	7
         *		|										|
         *		|										|
         *		|---------------------------------------|
         */

        //移出
        if (next == 1 || next == 7)
        {
            role.Effect.Add(12);
        }
        //移动
        else
        {

            role.Effect.Add(6);
        }

        role.Moving_time = Role1_Moving_time;
        role.Next_anchor_position = next;
    }

    public static string GetResName<D, T>(Dictionary<T, D> td, T key)
        where D : CSVDataBase
    {
        string resName = "";

        if (td.ContainsKey(key))
        {
            resName = td[key].E_name;
        }

        return resName;
    }
    public static int GetResID<D, T>(Dictionary<T, D> td, string ename)
        where D : CSVDataBase
    {
        int id = 0;

        foreach (var item in td.Values)
        {
            if (ename == item.E_name)
            {
                id = item.Id;
                break;
            }
        }

        return id;
    }
    public static void DicAdd<K, D>(ref Dictionary<K, List<D>> dic, K key, D value)
    {
        if (value == null)
        {
            return;
        }
        if (!dic.ContainsKey(key))
        {
            dic.Add(key, new List<D>());
        }
        //dic[key].Clear();
        dic[key].Add(value);
    }
    public static void DicAdd<K, D>(ref Dictionary<K, D> dic, K key, D value)
    {
        if (value == null)
        {
            return;
        }
        if (!dic.ContainsKey(key))
        {
            dic.Add(key, value);
        }
        else
        {
            dic[key] = value;
        }
    }

    #endregion

    #endregion

}
