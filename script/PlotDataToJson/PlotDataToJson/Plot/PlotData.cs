﻿using Data;
using Plus;
using Plus.Data;
using Plus.GIO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plot
{
    public class PlotData : Singleton<PlotData>
    {
        public string PathBase = "Assets/Bundle/Data/CSV/Plot";
        public const string PathOldData = "PlotData";
        public const string PathNumDir = "{0}/Plot{1}";
        public const string SectionPathDir = "{0}/Section/Section{1}";
        public const string ChapterPathDir = "{0}/Chapter/Chapter{1}";

        protected virtual string _path => PathBase;

        #region 基础数据

        #region 可更改
        #region 对话数据

        public Dictionary<int, List<dia_sessionCSVData>> session = new Dictionary<int, List<dia_sessionCSVData>>();

        #endregion

        #region 场次逻辑

        public Dictionary<int, List<func_bgCSVData>> funcBg = new Dictionary<int, List<func_bgCSVData>>();
        public Dictionary<int, List<func_cgCSVData>> funcCg = new Dictionary<int, List<func_cgCSVData>>();
        public Dictionary<int, List<func_dialog_boxCSVData>> funcDialogBox = new Dictionary<int, List<func_dialog_boxCSVData>>();
        public Dictionary<int, List<func_roleCSVData>> funcRole = new Dictionary<int, List<func_roleCSVData>>();
        

        #endregion
        #endregion

        #endregion

        #region 对话剧情表

        public void SaveData(string writePath,string flag)
        {
            PathBase = writePath;
            DeleteData(session);

            #region 对话数据
            SaveData(session, flag);
            #endregion

            #region 场次逻辑
            SaveData(funcBg, flag);
            SaveData(funcCg, flag);
            SaveData(funcDialogBox, flag);
            SaveData(funcRole, flag);
            #endregion

            Console.WriteLine("创建完成");

            //JavaScriptSerializer javaScript = new JavaScriptSerializer();
            //string str = javaScript.Serialize(funcRole);
            //Debug.LogError(str);
        }
        public void SaveData<T>(Dictionary<int, T> dicT)
           where T : CSVDataBase, new()
        {
            DataBasePlus.SaveData(dicT, _path, DirectoryType.Assets);
        }
        /// <summary>
        /// 保存数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dicT"></param>
        /// <param name="flag"></param>
        public void SaveData<T>(Dictionary<int, List<T>> dicT,string flag)
            where T : CSVDataBase, new()
        {
            switch (flag)
            {
                case "0": //json
                    SaveJsonData(dicT, PathBase, PathNumDir);
                    break;
                case "1": //csv
                    SaveData(dicT, PathBase, PathNumDir);
                    break;
                default:
                    break;
            }

        }
        #endregion

        #region 获取数据


        #endregion

        #region 分布储存

        /// <summary>
        /// 分布储存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dicT"></param>
        /// <param name="dirPathBase"></param>
        /// <param name="ditPath"></param>
        public void SaveData<T>(Dictionary<int, List<T>> dicT, string dirPathBase, string ditPath)
            where T : CSVDataBase, new()
        {
            List<int> keys = new List<int>();
            List<string> values = new List<string>();

            foreach (var item in dicT)
            {

                int count = item.Value.Count;

                List<List<string>> strs = new List<List<string>>();

                for (int i = 0; i < count; i++)
                {
                    strs.Add(item.Value[i].Get());
                }
                string str = GetTxt(strs);

                keys.Add(item.Key);
                values.Add(str);
            }

            int keyCount = keys.Count;

            for (int i = 0; i < keyCount; i++)
            {
                string dirPath = string.Format(ditPath, dirPathBase, keys[i]);
                DirectoryPlus.CreateDirectory(dirPath);

                string name = DataBasePlus.GetClassName<T>();
                string path = string.Format("{0}/{1}{2}", dirPath, name, keys[i]);

                SetTxtBy(path, values[i], DirectoryType.Assets);
            }
        }

        /// <summary>
        /// 分布储存
        /// Json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dicT"></param>
        /// <param name="pathBase"></param>
        /// <param name="pathNumDir"></param>
        private void SaveJsonData<T>(Dictionary<int, List<T>> dicT, string pathBase, string pathNumDir) where T : CSVDataBase, new()
        {
            List<int> keys = new List<int>();
            List<string> values = new List<string>();
            foreach (var item in dicT)
            {
                keys.Add(item.Key);
                values.Add(JsonConvert.SerializeObject(item.Value));
            }

            for (int i = 0; i < keys.Count; i++)
            {
                string dirPath = string.Format(pathNumDir, pathBase, keys[i]);
                DirectoryPlus.CreateDirectory(dirPath);

                string name = DataBasePlus.GetClassName<T>();
                string path = string.Format("{0}/{1}{2}", dirPath, name, keys[i]);

                SetJsonBy(path, values[i], DirectoryType.Assets);
            }
        }

        /// <summary>
        /// 储存剧情数据
        /// </summary>
        /// <param name="dataOld"></param>
        public void SavePlotChapterData(Dictionary<int, List<NewFunctionCSVData>> dataOld)
        {
            Dictionary<int, List<NewFunctionCSVData>> dataNow = new Dictionary<int, List<NewFunctionCSVData>>();
            
            SaveData(dataOld, PathOldData, SectionPathDir);
            SaveData(dataNow, PathOldData, ChapterPathDir);
        }

        public static void DicAdd<K, D>(ref Dictionary<K, List<D>> dic, K key, List<D> value)
        {
            if (value == null)
            {
                return;
            }
            if (!dic.ContainsKey(key))
            {
                dic.Add(key, new List<D>());
            }
            //dic[key].Clear();
            dic[key].AddRange(value);
        }

        #endregion

        #region 删除原数据

        private void DeleteData<T>(Dictionary<int, List<T>> dicT)
           where T : CSVDataBase, new()
        {
            List<int> ids = new List<int>();

            ids.AddRange(dicT.Keys);


            for (int i = 0; i < ids.Count; i++)
            {
                string path = string.Format(PathNumDir, PathBase, ids[i]);

                //System.IO.File.Delete(path);
                //删除文件及文件夹
                if (System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.Delete(path, true);
                }
            }
        }

        #endregion

        #region CSV.txt
        public static string GetTxt(List<List<string>> strs)
        {
            List<string> listStrs = new List<string>();
            for (int i = 0; i < strs.Count; i++)
            {
                string str = "";
                for (int j = 0; j < strs[i].Count; j++)
                {
                    if (j == 0)
                    {
                        str += strs[i][j];
                    }
                    else
                    {

                        str += ("," + strs[i][j]);
                    }
                }
                listStrs.Add(str);
            }
            return GetTxt(listStrs);
        }
        public static string GetTxt(List<string> strs)
        {
            string str = "";
            for (int i = 0; i < strs.Count; i++)
            {
                if (i == 0)
                {
                    str += strs[i];
                }
                else
                {

                    str += ("\r\n" + strs[i]);
                }
            }
            return str;
            //SetTxtBy(path, str, type);
        }
        public static void SetTxtBy(string path, string strs, DirectoryType type)
        {
            switch (type)
            {
                case DirectoryType.Assets:
                    Plus.GIO.FilePlus.WriterAssets($"{path}.txt", false, strs);
                    break;
                case DirectoryType.Resources:
                    Plus.GIO.FilePlus.WriterResources(path, false, strs);
                    break;
                default:
                    break;
            }
        }
        public static void SetJsonBy(string path, string strs, DirectoryType type)
        {
            switch (type)
            {
                case DirectoryType.Assets:
                    Plus.GIO.FilePlus.WriterAssets($"{path}.json", false, strs);
                    break;
                case DirectoryType.Resources:
                    Plus.GIO.FilePlus.WriterResources(path, false, strs);
                    break;
                default:
                    break;
            }
        }
        #endregion
    }

}
