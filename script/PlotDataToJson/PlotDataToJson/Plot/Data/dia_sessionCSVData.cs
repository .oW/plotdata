using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Data
{
	public class dia_sessionCSVData : CSVDataBase
	{
		//当前场次ID
		private int index;
		//下一场次ID
		private int next_index;
		//小结id
		private int section_id;
		//立绘或CG
		private int role_or_cg;
		//选项
		private List<string> Options;
		//跳转
		private List<int> nexts;
		/// <summary>
		/// 当前场次ID
		/// </summary>
		public int Index { get => index; set => index = value; }
		/// <summary>
		/// 下一场次ID
		/// </summary>
		public int Next_index { get => next_index; set => next_index = value; }
		/// <summary>
		/// 小结id
		/// </summary>
		public int Section_id { get => section_id; set => section_id = value; }
		/// <summary>
		/// 立绘或CG
		/// </summary>
		public int Role_or_cg { get => role_or_cg; set => role_or_cg = value; }
		/// <summary>
		/// 选项
		/// </summary>
		public List<string> Options1 { get => Options; set => Options = value; }
		/// <summary>
		/// 跳转
		/// </summary>
		public List<int> Nexts { get => nexts; set => nexts = value; }
		public override int GetImportantId => Section_id;
		public dia_sessionCSVData()
		{
		}
		protected override void OnSet(List<string> list)
		{
			int i = 4;
			index = sP.TByString<int>(list[++i]);
			next_index = sP.TByString<int>(list[++i]);
			section_id = sP.TByString<int>(list[++i]);
			role_or_cg = sP.TByString<int>(list[++i]);
			Options = sP.ListByString<string>(list[++i]);
			nexts = sP.ListByString<int>(list[++i]);
		}
		protected override List<string> OnGet(List<string> list)
		{
			list.Add(sP.StringByT(index));
			list.Add(sP.StringByT(next_index));
			list.Add(sP.StringByT(section_id));
			list.Add(sP.StringByT(role_or_cg));
			list.Add(sP.StringByList(Options));
			list.Add(sP.StringByList(nexts));
			return list;
		}
	}
}
