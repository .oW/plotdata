using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Data
{
	public class func_bgCSVData : CSVDataBase
	{
		//小节id
		private int section_id;
		//场次id
		private int session_id;
		//背景图片id
		private int bg_img_id;
		//音乐id
		private int music_id;
		//音效id
		private int sound_id;
		//全屏特效
		private int full_screen_effects;
		//特效
		private int effect;
		//标题
		private string title;
		/// <summary>
		/// 小节id
		/// </summary>
		public int Section_id { get => section_id; set => section_id = value; }
		/// <summary>
		/// 场次id
		/// </summary>
		public int Session_id { get => session_id; set => session_id = value; }
		/// <summary>
		/// 背景图片id
		/// </summary>
		public int Bg_img_id { get => bg_img_id; set => bg_img_id = value; }
		/// <summary>
		/// 音乐id
		/// </summary>
		public int Music_id { get => music_id; set => music_id = value; }
		/// <summary>
		/// 音效id
		/// </summary>
		public int Sound_id { get => sound_id; set => sound_id = value; }
		/// <summary>
		/// 全屏特效
		/// </summary>
		public int Full_screen_effects { get => full_screen_effects; set => full_screen_effects = value; }
		/// <summary>
		/// 特效
		/// </summary>
		public int Effect { get => effect; set => effect = value; }
		/// <summary>
		/// 标题
		/// </summary>
		public string Title { get => title; set => title = value; }
		public override int GetImportantId => Section_id;
		public func_bgCSVData()
		{
		}
		protected override void OnSet(List<string> list)
		{
			int i = 4;
			section_id = sP.TByString<int>(list[++i]);
			session_id = sP.TByString<int>(list[++i]);
			bg_img_id = sP.TByString<int>(list[++i]);
			music_id = sP.TByString<int>(list[++i]);
			sound_id = sP.TByString<int>(list[++i]);
			full_screen_effects = sP.TByString<int>(list[++i]);
			effect = sP.TByString<int>(list[++i]);
			title = sP.TByString<string>(list[++i]);
		}
		protected override List<string> OnGet(List<string> list)
		{
			list.Add(sP.StringByT(section_id));
			list.Add(sP.StringByT(session_id));
			list.Add(sP.StringByT(bg_img_id));
			list.Add(sP.StringByT(music_id));
			list.Add(sP.StringByT(sound_id));
			list.Add(sP.StringByT(full_screen_effects));
			list.Add(sP.StringByT(effect));
			list.Add(sP.StringByT(title));
			return list;
		}
	}
}
