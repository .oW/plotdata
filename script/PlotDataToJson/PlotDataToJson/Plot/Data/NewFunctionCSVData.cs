using Plot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
	public class NewFunctionCSVData : CSVDataBase
	{
		//小节id
		private int section_id;
		//场次id
		private int session_id;
		//下一场id
		private int next_id;
		//特效类型
		private int effect_type;
		//spine动画id
		private int type_spine;
		//动画id
		private int type_anim;
		//cg图片ID
		private int cg_image_id;
		//背景图片id
		private int bg_img_id;
		//音乐id
		private int music_id;
		//音效id
		private int sound_id;
		//全屏特效
		private int full_screen_effects;
		//背景特效
		private int effect;
		//标题文字
		private string title;
		//对话框图片id
		private int box_img_id;
		//特效
		private int box_effect;
		//人名框的位置
		private int name_box_po;
		//头像id
		private int avatar_id;
		//当前说话人的id
		private int speaker_id;
		//当前说话人的名称
		private string speaker_name;
		//对话内容
		private string conversation_content;
		//对话内容字号
		private int content_size;
		//配音文件
		private int Dubbing;
		//立绘图片id
		private int photo_id1;
		//初始位置
		private int initial_point1;
		//目标位置
		private int starting_point1;
		//出入场效果
		private int inout_Effect1;
		//特效
		private int photo_effect1;
		//小表情id
		private int emoticon_id1;
		//小表情对应位置
		private Vector2 emoticon_position1;
		//立绘面部表情id
		private int facial_expression1;
		//立绘图片id
		private int photo_id2;
		//初始位置
		private int initial_point2;
		//目标位置
		private int starting_point2;
		//出入场效果
		private int inout_Effect2;
		//特效
		private int photo_effect2;
		//小表情id
		private int emoticon_id2;
		//小表情对应位置
		private Vector2 emoticon_position2;
		//面部表情id
		private int facial_expression2;
		//立绘图片id
		private int photo_id3;
		//初始位置
		private int initial_point3;
		//目标位置
		private int starting_point3;
		//出入场效果
		private int inout_Effect3;
		//特效
		private int photo_effect3;
		//小表情id
		private int emoticon_id3;
		//小表情对应位置
		private Vector2 emoticon_position3;
		//面部表情id
		private int facial_expression3;
		//选项A
		private string Options_1;
		//跳转
		private int next_1;
		//选项B
		private string Options_2;
		//跳转
		private int next_2;
		//选项C
		private string Options_3;
		//跳转
		private int next_3;
		//选项D
		private string Options_4;
		//跳转
		private int next_4;
		//选项E
		private string Options_5;
		//跳转
		private int next_5;
		//选项F
		private string Options_6;
		//跳转
		private int next_6;
		//自动跳转
		private int automatic_jump;
		/// <summary>
		/// 小节id
		/// </summary>
		public int Section_id { get => section_id; set => section_id = value; }
		/// <summary>
		/// 场次id
		/// </summary>
		public int Session_id { get => session_id; set => session_id = value; }
		/// <summary>
		/// 下一场id
		/// </summary>
		public int Next_id { get => next_id; set => next_id = value; }
		/// <summary>
		/// 特效类型
		/// </summary>
		public int Effect_type { get => effect_type; set => effect_type = value; }
		/// <summary>
		/// spine动画id
		/// </summary>
		public int Type_spine { get => type_spine; set => type_spine = value; }
		/// <summary>
		/// 动画id
		/// </summary>
		public int Type_anim { get => type_anim; set => type_anim = value; }
		/// <summary>
		/// cg图片ID
		/// </summary>
		public int Cg_image_id { get => cg_image_id; set => cg_image_id = value; }
		/// <summary>
		/// 背景图片id
		/// </summary>
		public int Bg_img_id { get => bg_img_id; set => bg_img_id = value; }
		/// <summary>
		/// 音乐id
		/// </summary>
		public int Music_id { get => music_id; set => music_id = value; }
		/// <summary>
		/// 音效id
		/// </summary>
		public int Sound_id { get => sound_id; set => sound_id = value; }
		/// <summary>
		/// 全屏特效
		/// </summary>
		public int Full_screen_effects { get => full_screen_effects; set => full_screen_effects = value; }
		/// <summary>
		/// 背景特效
		/// </summary>
		public int Effect { get => effect; set => effect = value; }
		/// <summary>
		/// 标题文字
		/// </summary>
		public string Title { get => title; set => title = value; }
		/// <summary>
		/// 对话框图片id
		/// </summary>
		public int Box_img_id { get => box_img_id; set => box_img_id = value; }
		/// <summary>
		/// 特效
		/// </summary>
		public int Box_effect { get => box_effect; set => box_effect = value; }
		/// <summary>
		/// 人名框的位置
		/// </summary>
		public int Name_box_po { get => name_box_po; set => name_box_po = value; }
		/// <summary>
		/// 头像id
		/// </summary>
		public int Avatar_id { get => avatar_id; set => avatar_id = value; }
		/// <summary>
		/// 当前说话人的id
		/// </summary>
		public int Speaker_id { get => speaker_id; set => speaker_id = value; }
		/// <summary>
		/// 当前说话人的名称
		/// </summary>
		public string Speaker_name { get => speaker_name; set => speaker_name = value; }
		/// <summary>
		/// 对话内容
		/// </summary>
		public string Conversation_content { get => conversation_content; set => conversation_content = value; }
		/// <summary>
		/// 对话内容字号
		/// </summary>
		public int Content_size { get => content_size; set => content_size = value; }
		/// <summary>
		/// 配音文件
		/// </summary>
		public int Dubbing1 { get => Dubbing; set => Dubbing = value; }
		/// <summary>
		/// 立绘图片id
		/// </summary>
		public int Photo_id1 { get => photo_id1; set => photo_id1 = value; }
		/// <summary>
		/// 初始位置
		/// </summary>
		public int Initial_point1 { get => initial_point1; set => initial_point1 = value; }
		/// <summary>
		/// 目标位置
		/// </summary>
		public int Starting_point1 { get => starting_point1; set => starting_point1 = value; }
		/// <summary>
		/// 出入场效果
		/// </summary>
		public int Inout_Effect1 { get => inout_Effect1; set => inout_Effect1 = value; }
		/// <summary>
		/// 特效
		/// </summary>
		public int Photo_effect1 { get => photo_effect1; set => photo_effect1 = value; }
		/// <summary>
		/// 小表情id
		/// </summary>
		public int Emoticon_id1 { get => emoticon_id1; set => emoticon_id1 = value; }
		/// <summary>
		/// 小表情对应位置
		/// </summary>
		public Vector2 Emoticon_position1 { get => emoticon_position1; set => emoticon_position1 = value; }
		/// <summary>
		/// 立绘面部表情id
		/// </summary>
		public int Facial_expression1 { get => facial_expression1; set => facial_expression1 = value; }
		/// <summary>
		/// 立绘图片id
		/// </summary>
		public int Photo_id2 { get => photo_id2; set => photo_id2 = value; }
		/// <summary>
		/// 初始位置
		/// </summary>
		public int Initial_point2 { get => initial_point2; set => initial_point2 = value; }
		/// <summary>
		/// 目标位置
		/// </summary>
		public int Starting_point2 { get => starting_point2; set => starting_point2 = value; }
		/// <summary>
		/// 出入场效果
		/// </summary>
		public int Inout_Effect2 { get => inout_Effect2; set => inout_Effect2 = value; }
		/// <summary>
		/// 特效
		/// </summary>
		public int Photo_effect2 { get => photo_effect2; set => photo_effect2 = value; }
		/// <summary>
		/// 小表情id
		/// </summary>
		public int Emoticon_id2 { get => emoticon_id2; set => emoticon_id2 = value; }
		/// <summary>
		/// 小表情对应位置
		/// </summary>
		public Vector2 Emoticon_position2 { get => emoticon_position2; set => emoticon_position2 = value; }
		/// <summary>
		/// 面部表情id
		/// </summary>
		public int Facial_expression2 { get => facial_expression2; set => facial_expression2 = value; }
		/// <summary>
		/// 立绘图片id
		/// </summary>
		public int Photo_id3 { get => photo_id3; set => photo_id3 = value; }
		/// <summary>
		/// 初始位置
		/// </summary>
		public int Initial_point3 { get => initial_point3; set => initial_point3 = value; }
		/// <summary>
		/// 目标位置
		/// </summary>
		public int Starting_point3 { get => starting_point3; set => starting_point3 = value; }
		/// <summary>
		/// 出入场效果
		/// </summary>
		public int Inout_Effect3 { get => inout_Effect3; set => inout_Effect3 = value; }
		/// <summary>
		/// 特效
		/// </summary>
		public int Photo_effect3 { get => photo_effect3; set => photo_effect3 = value; }
		/// <summary>
		/// 小表情id
		/// </summary>
		public int Emoticon_id3 { get => emoticon_id3; set => emoticon_id3 = value; }
		/// <summary>
		/// 小表情对应位置
		/// </summary>
		public Vector2 Emoticon_position3 { get => emoticon_position3; set => emoticon_position3 = value; }
		/// <summary>
		/// 面部表情id
		/// </summary>
		public int Facial_expression3 { get => facial_expression3; set => facial_expression3 = value; }
		/// <summary>
		/// 选项A
		/// </summary>
		public string Options_11 { get => Options_1; set => Options_1 = value; }
		/// <summary>
		/// 跳转
		/// </summary>
		public int Next_1 { get => next_1; set => next_1 = value; }
		/// <summary>
		/// 选项B
		/// </summary>
		public string Options_21 { get => Options_2; set => Options_2 = value; }
		/// <summary>
		/// 跳转
		/// </summary>
		public int Next_2 { get => next_2; set => next_2 = value; }
		/// <summary>
		/// 选项C
		/// </summary>
		public string Options_31 { get => Options_3; set => Options_3 = value; }
		/// <summary>
		/// 跳转
		/// </summary>
		public int Next_3 { get => next_3; set => next_3 = value; }
		/// <summary>
		/// 选项D
		/// </summary>
		public string Options_41 { get => Options_4; set => Options_4 = value; }
		/// <summary>
		/// 跳转
		/// </summary>
		public int Next_4 { get => next_4; set => next_4 = value; }
		/// <summary>
		/// 选项E
		/// </summary>
		public string Options_51 { get => Options_5; set => Options_5 = value; }
		/// <summary>
		/// 跳转
		/// </summary>
		public int Next_5 { get => next_5; set => next_5 = value; }
		/// <summary>
		/// 选项F
		/// </summary>
		public string Options_61 { get => Options_6; set => Options_6 = value; }
		/// <summary>
		/// 跳转
		/// </summary>
		public int Next_6 { get => next_6; set => next_6 = value; }
		/// <summary>
		/// 自动跳转
		/// </summary>
		public int Automatic_jump { get => automatic_jump; set => automatic_jump = value; }
		public NewFunctionCSVData()
		{
		}
		protected override void OnSet(List<string> list)
		{
			int i = 4;
			section_id = sP.TByString<int>(list[++i]);
			session_id = sP.TByString<int>(list[++i]);
			next_id = sP.TByString<int>(list[++i]);
			effect_type = sP.TByString<int>(list[++i]);
			type_spine = sP.TByString<int>(list[++i]);
			type_anim = sP.TByString<int>(list[++i]);
			cg_image_id = sP.TByString<int>(list[++i]);
			bg_img_id = sP.TByString<int>(list[++i]);
			music_id = sP.TByString<int>(list[++i]);
			sound_id = sP.TByString<int>(list[++i]);
			full_screen_effects = sP.TByString<int>(list[++i]);
			effect = sP.TByString<int>(list[++i]);
			title = sP.TByString<string>(list[++i]);
			box_img_id = sP.TByString<int>(list[++i]);
			box_effect = sP.TByString<int>(list[++i]);
			name_box_po = sP.TByString<int>(list[++i]);
			avatar_id = sP.TByString<int>(list[++i]);
			speaker_id = sP.TByString<int>(list[++i]);
			speaker_name = sP.TByString<string>(list[++i]);
			conversation_content = sP.TByString<string>(list[++i]);
			content_size = sP.TByString<int>(list[++i]);
			Dubbing = sP.TByString<int>(list[++i]);
			photo_id1 = sP.TByString<int>(list[++i]);
			initial_point1 = sP.TByString<int>(list[++i]);
			starting_point1 = sP.TByString<int>(list[++i]);
			inout_Effect1 = sP.TByString<int>(list[++i]);
			photo_effect1 = sP.TByString<int>(list[++i]);
			emoticon_id1 = sP.TByString<int>(list[++i]);
			emoticon_position1 = sP.TByString<Vector2>(list[++i]);
			facial_expression1 = sP.TByString<int>(list[++i]);
			photo_id2 = sP.TByString<int>(list[++i]);
			initial_point2 = sP.TByString<int>(list[++i]);
			starting_point2 = sP.TByString<int>(list[++i]);
			inout_Effect2 = sP.TByString<int>(list[++i]);
			photo_effect2 = sP.TByString<int>(list[++i]);
			emoticon_id2 = sP.TByString<int>(list[++i]);
			emoticon_position2 = sP.TByString<Vector2>(list[++i]);
			facial_expression2 = sP.TByString<int>(list[++i]);
			photo_id3 = sP.TByString<int>(list[++i]);
			initial_point3 = sP.TByString<int>(list[++i]);
			starting_point3 = sP.TByString<int>(list[++i]);
			inout_Effect3 = sP.TByString<int>(list[++i]);
			photo_effect3 = sP.TByString<int>(list[++i]);
			emoticon_id3 = sP.TByString<int>(list[++i]);
			emoticon_position3 = sP.TByString<Vector2>(list[++i]);
			facial_expression3 = sP.TByString<int>(list[++i]);
			Options_1 = sP.TByString<string>(list[++i]);
			next_1 = sP.TByString<int>(list[++i]);
			Options_2 = sP.TByString<string>(list[++i]);
			next_2 = sP.TByString<int>(list[++i]);
			Options_3 = sP.TByString<string>(list[++i]);
			next_3 = sP.TByString<int>(list[++i]);
			Options_4 = sP.TByString<string>(list[++i]);
			next_4 = sP.TByString<int>(list[++i]);
			Options_5 = sP.TByString<string>(list[++i]);
			next_5 = sP.TByString<int>(list[++i]);
			Options_6 = sP.TByString<string>(list[++i]);
			next_6 = sP.TByString<int>(list[++i]);
			automatic_jump = sP.TByString<int>(list[++i]);
		}
		protected override List<string> OnGet(List<string> list)
		{
			list.Add(sP.StringByT(section_id));
			list.Add(sP.StringByT(session_id));
			list.Add(sP.StringByT(next_id));
			list.Add(sP.StringByT(effect_type));
			list.Add(sP.StringByT(type_spine));
			list.Add(sP.StringByT(type_anim));
			list.Add(sP.StringByT(cg_image_id));
			list.Add(sP.StringByT(bg_img_id));
			list.Add(sP.StringByT(music_id));
			list.Add(sP.StringByT(sound_id));
			list.Add(sP.StringByT(full_screen_effects));
			list.Add(sP.StringByT(effect));
			list.Add(sP.StringByT(title));
			list.Add(sP.StringByT(box_img_id));
			list.Add(sP.StringByT(box_effect));
			list.Add(sP.StringByT(name_box_po));
			list.Add(sP.StringByT(avatar_id));
			list.Add(sP.StringByT(speaker_id));
			list.Add(sP.StringByT(speaker_name));
			list.Add(sP.StringByT(conversation_content));
			list.Add(sP.StringByT(content_size));
			list.Add(sP.StringByT(Dubbing));
			list.Add(sP.StringByT(photo_id1));
			list.Add(sP.StringByT(initial_point1));
			list.Add(sP.StringByT(starting_point1));
			list.Add(sP.StringByT(inout_Effect1));
			list.Add(sP.StringByT(photo_effect1));
			list.Add(sP.StringByT(emoticon_id1));
			list.Add(sP.StringByT(emoticon_position1));
			list.Add(sP.StringByT(facial_expression1));
			list.Add(sP.StringByT(photo_id2));
			list.Add(sP.StringByT(initial_point2));
			list.Add(sP.StringByT(starting_point2));
			list.Add(sP.StringByT(inout_Effect2));
			list.Add(sP.StringByT(photo_effect2));
			list.Add(sP.StringByT(emoticon_id2));
			list.Add(sP.StringByT(emoticon_position2));
			list.Add(sP.StringByT(facial_expression2));
			list.Add(sP.StringByT(photo_id3));
			list.Add(sP.StringByT(initial_point3));
			list.Add(sP.StringByT(starting_point3));
			list.Add(sP.StringByT(inout_Effect3));
			list.Add(sP.StringByT(photo_effect3));
			list.Add(sP.StringByT(emoticon_id3));
			list.Add(sP.StringByT(emoticon_position3));
			list.Add(sP.StringByT(facial_expression3));
			list.Add(sP.StringByT(Options_1));
			list.Add(sP.StringByT(next_1));
			list.Add(sP.StringByT(Options_2));
			list.Add(sP.StringByT(next_2));
			list.Add(sP.StringByT(Options_3));
			list.Add(sP.StringByT(next_3));
			list.Add(sP.StringByT(Options_4));
			list.Add(sP.StringByT(next_4));
			list.Add(sP.StringByT(Options_5));
			list.Add(sP.StringByT(next_5));
			list.Add(sP.StringByT(Options_6));
			list.Add(sP.StringByT(next_6));
			list.Add(sP.StringByT(automatic_jump));
			return list;
		}
	}
}
