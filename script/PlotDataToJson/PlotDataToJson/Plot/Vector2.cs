﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plot
{
    public class Vector2
    {
        internal static Vector2 zero = new Vector2(0,0);
        internal static Vector2 one = new Vector2(1, 1);
        public float x;
        public float y;

        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }


        public override string ToString()
        {
            return string.Format("{0},{1}",x,y);
        }
    }
}
