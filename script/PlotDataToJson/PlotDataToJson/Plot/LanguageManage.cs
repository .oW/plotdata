﻿using Data;
using Plus;
using Plus.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 语言管理类
/// </summary>
public class LanguageManage : Singleton<LanguageManage>
{
    public void Init(string readPath)
    {
        lang = lang ?? DataBasePlus.GetDicStr<languageCNCSVData>(readPath);
    }

    /// <summary>
    /// 中文
    /// </summary>
    private Dictionary<string, languageCNCSVData> lang;
    public Dictionary<string, languageCNCSVData> Lang
    {
        get
        {
            if (lang == null)
            {
                lang = DataBasePlus.GetDicStr<languageCNCSVData>();
            }
            return lang;
        }
    }

    /// <summary>
    /// 根据文字获取多语言ID
    /// </summary>
    /// <returns></returns>
    public string GetLanguageStrIdEditor(string txt)
    {
        string value = txt;

        if (Lang.ContainsKey(txt))
        {
            value = value.Replace("\"","\\\"");
            value = Lang[txt].Id.ToString();
        }
        return value;

    }

}

