import os,codecs

# 生成 csv
def ExportCsvData(path):
    rPath = path + "\..\..\PlotOriginalTable";
    wPath = path +"\..\..\Plot";
    para = "%s \"%s\" \"%s\" "%("NewFunctionCsvExportData.bat",rPath,wPath);
    os.system(para);
    return;

# 生成 多语言
def ChangeLanguage():
    
    plotPath = "..\..\Plot";
    languagePath = "..\..\Plot";
    
    titleIdStart = "40000";
    titleIdEnd = "49999";
    OptionsStart = "50000";
    OptionsEnd = "59999";
    nameIdStart = "60000";
    nameIdEnd = "60999";
    textIdStart = "61000";
    textIdEnd = "1000000";
    
    idStr = titleIdStart + "-" + titleIdEnd
    idStr = idStr + "+" + OptionsStart + "-" + OptionsEnd
    idStr = idStr + "+" + nameIdStart + "-" + nameIdEnd
    idStr = idStr + "+" + textIdStart + "-" + textIdEnd
    
    para = "%s \"%s\" \"%s\" \"%s\" "%("MltiLanguage.bat",plotPath,languagePath,idStr);
    os.system(para);
    return;

# 分布式储存
def ExportData(wPath,flag):
    rPath = "..\..\Plot";
    para = "%s \"%s\" \"%s\" \"%s\" "%("PlotExportData.bat",rPath,wPath,flag);
    os.system(para);
    return;
# json转换
def ExportJsonData():
    wPath = "..\..\Plot\PlotJson";
    flag = "0";#json
    ExportData(wPath,flag);
    return;
# csv转换
def ExportTxtData():
    wPath = "..\..\Plot\PlotTxt";
    flag = "1";#csv
    ExportData(wPath,flag);
    return;

# excel 转 csv
ExportCsvData(os.getcwd());
# 多语言整理
ChangeLanguage();

# 调用
# json转换
ExportJsonData();
# # csv转换
ExportTxtData();
