﻿using Game.Plus.GIO;
using Game.Plus.GIO.Data;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    public class ExcelManage
    {
        public string readPath;
        public string writePath;

        List<string> paths = new List<string>();
        public int count;
        public const string newFunction = "NewFunction";
        public const string language = "languageCN";

        public void Init(string readPath, string writePath)
        {
            this.readPath = readPath;
            this.writePath = writePath;
            paths.Clear();

            List<FileData> csvDatas = DirectoryPlus.GetTypes(DirectoryType.All, FileType.csv, writePath);
            //for (int i = csvDatas.Count - 1; i >=0 ; i--)
            //{
            //    if (csvDatas[i].name.StartsWith(newFunction) && csvDatas[i].name.StartsWith(language))
            //    {
            //    }
            //    else
            //    {
            //        csvDatas.RemoveAt(i);
            //    }
            //}
            DeleteAllTypeFile(csvDatas);




            Console.WriteLine(csvDatas.Count);

            List<FileData>  readDatas = DirectoryPlus.GetTypes(DirectoryType.All, FileType.xlsx, readPath);

            Function(readDatas);




            Console.WriteLine(readDatas.Count);

            List<FileData> newXlkDatas = DirectoryPlus.GetTypes(DirectoryType.All, FileType.xlk, writePath);

            XlkChange(newXlkDatas);

            Console.WriteLine(newXlkDatas.Count);




            List<FileData> newCsvDatas = DirectoryPlus.GetTypes(DirectoryType.All, FileType.csv, writePath);

            CSVChange(newCsvDatas);

            Console.WriteLine(newCsvDatas.Count);
        }

        #region 另存为

        public void Function(List<FileData> fileDatas)
        {

            //应用程序
            Application app = new Application();

            for (int i = 0; i < fileDatas.Count; i++)
            {
                FileData fileData = fileDatas[i];
                Workbook wbk;
                try
                {
                    //工作簿
                    wbk = app.Workbooks.Open(fileData.path);

                    ExcleTest(wbk);

                    wbk.Close(true);

                }
                catch (Exception e)
                {
                    Console.WriteLine(string.Format("\n{0} 出现问题\n{1}\n",fileData.path,e.ToString()));
                }
            }
            //退出
            app.Quit();
            //释放
            System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
        }

        public void ExcleTest(Workbook wbk)
        {
            List<string> labels = new List<string>();

            foreach (Worksheet item in wbk.Worksheets)
            {
                labels.Add(item.Name);
                //Console.WriteLine(item.Name);
            }
            try
            {
                for (int i = 0; i < labels.Count; i++)
                {
                    string key = labels[i];
                    Worksheet item = wbk.Worksheets[key];

                    string path = "";
                    if (item.Name.StartsWith(newFunction))
                    {
                        ++count;
                        path = string.Format(@"{0}\{1}{2}.csv", writePath, item.Name, count);
                    }else if (item.Name.StartsWith(language))
                    {
                        path = string.Format(@"{0}\{1}.csv", writePath, language);
                    }
                    else
                    {
                        continue;
                    }

                    //if (paths.IndexOf(path) != -1)
                    //{
                    //    path = writePath + wbk.Name + count + ".csv";
                    //}
                    if (paths.IndexOf(path) == -1)
                    {
                        paths.Add(path);
                        item.SaveAs(path, XlFileFormat.xlCSV,null,null,false,true, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlOtherSessionChanges);//,  null, null,false,false,XlSaveAsAccessMode.xlExclusive, XlSaveConflictResolution.xlOtherSessionChanges,);
                    }
                    Console.WriteLine(path);

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("报错了{0}",e.ToString()));
            }
        }

        #endregion

        #region 更改备份文件

        public void XlkChange(List<FileData> files)
        {


            for (int i = 0; i < files.Count; i++)
            {
                try
                {
                    string path = files[i].path;
                    string str = File.ReadAllText(path, Encoding.Default);

                    path = path.Substring(0, path.LastIndexOf(" 的备份.xlk"));

                    File.WriteAllText(path + ".csv", str, Encoding.UTF8);
                    Console.WriteLine("备份保存成功\t" + files[i].path);
                }
                catch (Exception)
                {
                    Console.WriteLine("备份保存失败\t" + files[i].path);
                }
            }

            DeleteAllTypeFile(files);
        }

        #endregion

        #region 转编码

        /// <summary>
        /// 转编码
        /// </summary>
        /// <param name="csvDatas"></param>
        public void CSVChange(List<FileData> csvDatas)
        {
            for (int i = 0; i < csvDatas.Count; i++)
            {
                try
                {
                    string path = csvDatas[i].path;
                    string str = File.ReadAllText(path, Encoding.Default);

                    //删除空数据
                    DeleteNullData(ref str);

                    File.WriteAllText(csvDatas[i].path, str, Encoding.UTF8);
                    Console.WriteLine("转换成功\t" + csvDatas[i].path);
                }
                catch (Exception)
                {
                    Console.WriteLine("转换失败\t" + csvDatas[i].path);
                }
            }
        }

        #endregion

        #region 删除空数据

        /// <summary>
        /// 删除空数据
        /// </summary>
        /// <param name="str"></param>
        public void DeleteNullData(ref string str)
        {
            List<string> strs = new List<string>();
            strs.AddRange(str.Split("\r\n".ToArray()));

            for (int i = strs.Count - 1; i >= 0; i--)
            {
                if (strs[i] == "" || strs[i] == null || strs[i].StartsWith(","))
                {
                    strs.RemoveAt(i);
                }
            }
            str = "";

            for (int i = 0; i < strs.Count; i++)
            {
                if(i == 0)
                {
                    str = strs[i];
                }
                else
                {
                    str = string.Format("{0}\r\n{1}", str, strs[i]);
                }
            }
        }

        #endregion

        #region 删除文件

        public void DeleteAllTypeFile(List<FileData> files)
        {
            for (int i = 0; i < files.Count; i++)
            {
                File.Delete(files[i].path);
            }
        }

        #endregion

        #region Test01


        //Console.WriteLine(key.Count);

        //for (int i = 0; i < key.Count; i++)
        //{
        //    Console.WriteLine(key[i]);
        //    //工作表
        //    Worksheet wsh = wbk.Sheets[key[i]];
        //    try
        //    {
        //        int count = wsh.Index;
        //        Console.WriteLine(string.Format("*******************{0}", count));
        //        foreach (Range item in wsh.Cells)
        //        {
        //            if(item.Value == null)
        //            {

        //                break;
        //            }
        //            for (int j = 1; j <= count; j++)
        //            {
        //                if(item[j] == null)
        //                {
        //                    break;
        //                }
        //                Console.WriteLine(string.Format("*******************{0}", item[j].Count));
        //            }
        //            //Console.WriteLine(string.Format("*******************{0}", item[1].Value.ToString()));
        //            //Console.WriteLine(string.Format("*******************{0}", item[2].Value.ToString()));
        //            //Console.WriteLine(string.Format("*******************{0}", item[3].Value.ToString()));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //    }

        //foreach (dynamic item in wsh.Cells)
        //{

        //    if (item == null)
        //    {
        //        break;
        //    }
        //    if (item.Value == null)
        //    {
        //        break;
        //    }
        //    Console.WriteLine(string.Format("*******************{0}", item.Value.ToString()));
        //    //try
        //    //{
        //    //    foreach (dynamic item2 in wsh.)
        //    //    {
        //    //        dynamic obj = item2;
        //    //        if (obj == null)
        //    //        {
        //    //            break;
        //    //        }
        //    //        dynamic obj2 = item2.Value;
        //    //        if (obj2 == null)
        //    //        {
        //    //            break;
        //    //        }

        //    //        Console.WriteLine(string.Format("___________________{0}", obj2.ToString()));
        //    //    }
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    Console.WriteLine(e.ToString());
        //    //}

        //}


        //Console.WriteLine(key[i] + "Over");

        //读取
        //string str = wsh.Cells[1, 1].Value.ToString();

        //Console.WriteLine(str);

        //写入，索引以1开始
        //wsh.Cells[2, 1] = "str";
        //}


        //保存
        //wbk.Save();
        ///// <summary>
        ///// 简单操作Excel文件
        ///// </summary>
        ///// <param name="excelPath">excel 文件路径</param>
        ///// <returns></returns>
        //public void ExcelOp(string excelPath)
        //{
        //    string ExcelFilePath = excelPath.Trim();
        //    //set columns
        //    Dictionary<string, string> dic = new Dictionary<string, string>();
        //    dic.Add("订单号", "A");//
        //    dic.Add("数量", "B");

        //    Excel.Application excel = new Excel.Application();
        //    Excel.Workbook wb = null;
        //    excel.Visible = false;//设置调用引用的 Excel文件是否可见
        //    excel.DisplayAlerts = false;
        //    wb = excel.Workbooks.Open(ExcelFilePath);
        //    Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets[1]; //索引从1开始 //(Excel.Worksheet)wb.Worksheets["SheetName"];
        //    int rowCount = 0;//有效行，索引从1开始
        //    try
        //    {
        //        rowCount = ws.UsedRange.Rows.Count;//赋值有效行

        //        string ordernum = string.Empty;
        //        string count = string.Empty;
        //        //循环行
        //        for (int i = 1; i <= rowCount; i++)//
        //        {
        //            if (ws.Rows[i] != null)
        //            {
        //                ordernum = ws.Cells[i, dic["订单号"]].Value2.ToString();//取单元格值
        //                count = ws.Cells[i, dic["数量"]].Value2.ToString();//ws.Cells[i, 2].Value2.ToString();
        //            }
        //        }
        //        //循环列
        //        for (int i = 1; i <= ws.UsedRange.Columns.Count; i++)
        //        {
        //            //ws.Columns[i]
        //        }
        //    }
        //    catch (Exception ex) {
        //        //XtraMessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error); 
        //        Console.WriteLine(ex.ToString());
        //    }
        //    finally
        //    {
        //        ClosePro(excelPath, excel, wb);
        //    }
        //}

        ///// <summary>
        ///// 关闭Excel进程
        ///// </summary>
        ///// <param name="excelPath"></param>
        ///// <param name="excel"></param>
        ///// <param name="wb"></param>
        //public void ClosePro(string excelPath, Excel.Application excel, Excel.Workbook wb)
        //{
        //    Process[] localByNameApp = Process.GetProcessesByName(excelPath);//获取程序名的所有进程
        //    if (localByNameApp.Length > 0)
        //    {
        //        foreach (var app in localByNameApp)
        //        {
        //            if (!app.HasExited)
        //            {
        //                #region
        //                ////设置禁止弹出保存和覆盖的询问提示框   
        //                //excel.DisplayAlerts = false;
        //                //excel.AlertBeforeOverwriting = false;

        //                ////保存工作簿   
        //                //excel.Application.Workbooks.Add(true).Save();
        //                ////保存excel文件   
        //                //excel.Save("D:" + "\\test.xls");
        //                ////确保Excel进程关闭   
        //                //excel.Quit();
        //                //excel = null; 
        //                #endregion
        //                app.Kill();//关闭进程  
        //            }
        //        }
        //    }
        //    if (wb != null)
        //        wb.Close(true, Type.Missing, Type.Missing);
        //    excel.Quit();
        //    // 安全回收进程
        //    System.GC.GetGeneration(excel);
        //} 
        #endregion
    }
}
