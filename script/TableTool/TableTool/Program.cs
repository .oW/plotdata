﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                ExcelManage e = new ExcelManage();
                string readPath = args[0];
                string writePath = args[1];
                e.Init(readPath, writePath);
                //e.Init(@"D:\Users\OM\source\repos\PlotDataToJson\PlotOriginalTable", @"D:\Users\OM\source\repos\PlotDataToJson\Plot");
                //Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
